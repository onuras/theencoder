# theencoder

x265 and x264 helper to encode videos based on
[scene rules](https://scenerules.org/html/2020_X265.html).


### Usage

```
$ python -m theencoder -h
usage: __main__.py [-h] [-t TITLE] [--threads THREADS]
                   [--map-subtitles MAP_SUBTITLES [MAP_SUBTITLES ...]]
                   [--enable-remux] [--skip-1080p] [--skip-720p] [--x264]
                   [--container {mkv,mp4}] [--encoder-1080p {libx265,libx264}]
                   [--preset-1080p PRESET_1080P] [--crf-1080p CRF_1080P]
                   [--filters-1080p FILTERS_1080P]
                   [--audio-bitrate-1080p AUDIO_BITRATE_1080P]
                   [--encoder-720p {libx265,libx264}]
                   [--preset-720p PRESET_720P] [--crf-720p CRF_720P]
                   [--filters-720p FILTERS_720P]
                   [--audio-bitrate-720p AUDIO_BITRATE_720P] [--copy-orig]
                   [--move-orig] [--dry-run] [--quiet]
                   input_filename output_directory

positional arguments:
  input_filename
  output_directory

options:
  -h, --help            show this help message and exit
  -t TITLE, --title TITLE
                        movie title (default is input_filename)
  --threads THREADS     number of encoder threads (default is 2)
  --map-subtitles MAP_SUBTITLES [MAP_SUBTITLES ...]
                        include subtitle tracks in encode
  --enable-remux        create copy of original file and add Dolby Digital
                        Compatibility Track
  --skip-1080p          skip encoding 1080p
  --skip-720p           skip encoding 720p
  --x264                use x264 preset for encodes. This overrides encoder
                        options and sets codec to libx264 andcrf to 23 for
                        1080p and 24 for 720p. Also sets preset to veryslow
  --container {mkv,mp4}
                        multimedia container (default container is mkv)
  --encoder-1080p {libx265,libx264}
                        1080p encoder (default is libx265)
  --preset-1080p PRESET_1080P
                        1080p preset (default is slow)
  --crf-1080p CRF_1080P
                        1080p Constant Rate Factor (default is 20)
  --filters-1080p FILTERS_1080P
                        1080p filters
  --audio-bitrate-1080p AUDIO_BITRATE_1080P
                        1080p audio bitrate (default is 192k)
  --encoder-720p {libx265,libx264}
                        720p encoder (default is libx265)
  --preset-720p PRESET_720P
                        720p preset (default is slow)
  --crf-720p CRF_720P   1080p Constant Rate Factor (default is 22)
  --filters-720p FILTERS_720P
                        720p filters
  --audio-bitrate-720p AUDIO_BITRATE_720P
                        720p audio bitrate (default is 160k)
  --copy-orig           copy input file to output directory
  --move-orig           move input file to output directory
  --dry-run             simulate an encode
  --quiet               quiet output
```
