import argparse
import logging
import sys
from . import Encoder, EncoderOptions


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--title', help='movie title (default is input_filename)')
    parser.add_argument(
        '--threads',
        help='number of encoder threads (default is 2)',
        type=int,
        default=2,
    )
    parser.add_argument(
        '--map-subtitles', help='include subtitle tracks in encode', type=int, nargs='+'
    )
    parser.add_argument(
        '--enable-remux',
        help='create copy of original file and add Dolby Digital Compatibility Track',
        action='store_true',
    )
    parser.add_argument('--skip-1080p', help='skip encoding 1080p', action='store_true')
    parser.add_argument('--skip-720p', help='skip encoding 720p', action='store_true')
    parser.add_argument(
        '--x264',
        help='use x264 preset for encodes. '
        'This overrides encoder options and sets codec to libx264 and'
        'crf to 23 for 1080p and 24 for 720p. Also sets preset to veryslow',
        action='store_true',
    )
    parser.add_argument(
        '--container',
        help='multimedia container (default container is mkv)',
        choices=['mkv', 'mp4'],
        default='mkv',
    )

    parser.add_argument(
        '--encoder-1080p',
        help='1080p encoder (default is libx265)',
        choices=['libx265', 'libx264'],
        default='libx265',
    )
    parser.add_argument(
        '--preset-1080p', help='1080p preset (default is slow)', default='slow'
    )
    parser.add_argument(
        '--crf-1080p',
        help='1080p Constant Rate Factor (default is 20)',
        type=int,
        default=20,
    )
    parser.add_argument(
        '--filters-1080p',
        help='1080p filters',
    )
    parser.add_argument(
        '--audio-bitrate-1080p',
        help='1080p audio bitrate (default is 192k)',
        default='192k',
    )

    parser.add_argument(
        '--encoder-720p',
        help='720p encoder (default is libx265)',
        choices=['libx265', 'libx264'],
        default='libx265',
    )
    parser.add_argument(
        '--preset-720p', help='720p preset (default is slow)', default='slow'
    )
    parser.add_argument(
        '--crf-720p',
        help='1080p Constant Rate Factor (default is 22)',
        type=int,
        default=22,
    )
    parser.add_argument(
        '--filters-720p',
        help='720p filters',
        default='format=yuv420p',
    )
    parser.add_argument(
        '--audio-bitrate-720p',
        help='720p audio bitrate (default is 160k)',
        default='160k',
    )

    parser.add_argument(
        '--copy-orig', help='copy input file to output directory', action='store_true'
    )
    parser.add_argument(
        '--move-orig', help='move input file to output directory', action='store_true'
    )
    parser.add_argument('--dry-run', help='simulate an encode', action='store_true')
    parser.add_argument('--quiet', help='quiet output', action='store_true')

    parser.add_argument('input_filename')
    parser.add_argument('output_directory')

    args = parser.parse_args()

    logging.basicConfig(
        format='%(asctime)s %(levelname)s %(message)s',
        level=logging.DEBUG if not args.quiet else logging.INFO,
    )

    encoders = {
        '1080p': EncoderOptions(
            width=1920,
            height=1080,
            crf=args.crf_1080p if not args.x264 else 23,
            additional_filters=args.filters_1080p,
            encoder=args.encoder_1080p if not args.x264 else 'libx264',
            preset=args.preset_1080p if not args.x264 else 'veryslow',
            audio_bitrate=args.audio_bitrate_1080p,
            skip=args.skip_1080p,
        ),
        '720p': EncoderOptions(
            width=1280,
            height=720,
            crf=args.crf_720p if not args.x264 else 24,
            additional_filters=args.filters_720p,
            encoder=args.encoder_720p if not args.x264 else 'libx264',
            preset=args.preset_720p if not args.x264 else 'veryslow',
            audio_bitrate=args.audio_bitrate_720p,
            skip=args.skip_720p,
        ),
    }

    ret = Encoder.from_dict({**vars(args), 'encoders': encoders}).encode()
    sys.exit(ret)
