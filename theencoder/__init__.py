from dataclasses import dataclass, field
from pathlib import Path
import multiprocessing
import logging
import shutil
import subprocess
import inspect


@dataclass
class EncoderOptions:
    width: int
    height: int
    crf: int
    additional_filters: str | None = None
    encoder: str = 'libx265'
    preset: str = 'slow'
    audio_codec: str = 'ac3'
    audio_channels: int = 2
    audio_bitrate: str = '160k'
    skip: bool = False

    def __post_init__(self):
        self.filters = ','.join(
            [
                'scale=w={}:h={}:force_original_aspect_ratio=decrease'.format(
                    self.width, self.height
                ),
                self.additional_filters or '',
            ]
        )


@dataclass
class Encoder:
    title: str
    input_filename: Path
    output_directory: Path
    encoders: dict[str, EncoderOptions]
    enable_remux: bool = False
    map_subtitles: list[int] = field(default_factory=lambda: [])
    tune: str | None = None
    container: str = 'mkv'
    threads: int = 2
    dry_run: bool = False
    docker_image: str = 'linuxserver/ffmpeg:version-6.1-cli'
    copy_orig: bool = False
    move_orig: bool = False

    @classmethod
    def from_dict(cls, d):
        return cls(
            **{k: v for k, v in d.items() if k in inspect.signature(cls).parameters}
        )

    def __post_init__(self):
        self.cwd = Path.cwd()
        self.input_filename = Path(self.input_filename).absolute()
        self.input_directory = self.input_filename.parent
        self.output_directory = Path(self.output_directory).absolute()

        if not self.title:
            self.title = self.input_filename.stem

        if not self.input_filename.exists() or not self.input_filename.is_file():
            raise Exception(f'{self.input_filename} not exists')

        if not self.output_directory.exists():
            logging.debug(f'Creating output directory: {self.output_directory}')
            self.output_directory.mkdir()

        # check requisites
        for cmd in ['mediainfo', 'docker']:
            if not shutil.which(cmd):
                raise Exception(f'{cmd} not found')

    def _create_mediainfo(self, input_path, output_path):
        logging.info(f'Creating mediainfo for: {input_path}')
        if not self.dry_run:
            with output_path.open('w') as fp:
                subprocess.run(
                    ['mediainfo', input_path],
                    stdout=fp,
                    stderr=fp,
                    stdin=subprocess.DEVNULL,
                )

    def _generate_output_filename(self, tag, extension):
        return self.output_directory.joinpath(
            self.title + ' - ' + tag + '.' + extension
        )

    def _run_ffmpeg(self, tag, *options):
        output_filename = self._generate_output_filename(tag, self.container)
        log_filename = self._generate_output_filename(tag, 'log')
        logging.info(f'Encoding {tag}: {output_filename}')
        cmd = (
            [
                'docker', 'run', '--rm', '-i',
                '-v', '{}:{}'.format(self.input_directory, self.input_directory),
                '-v', '{}:{}'.format(self.output_directory, self.output_directory),
                '-v', '{}:{}'.format(self.cwd, self.cwd),
                '-w', str(self.cwd),
                self.docker_image,
                '-i', str(self.input_filename),
            ]
            + list(options)
            + ['-y', str(output_filename)]
        )
        logging.debug(
            'Running "{}" and logging outputs into: {}'.format(
                ' '.join(cmd), str(log_filename)
            )
        )
        if self.dry_run:
            cmd.insert(0, 'echo')
        with log_filename.open('w') as fp:
            print(' '.join(cmd), file=fp)
            ret = subprocess.call(cmd, stdout=fp, stderr=fp, stdin=subprocess.DEVNULL)
            # TODO: Add error handling here
            if ret != 0:
                logging.error(
                    f'Failed to encode {tag}, please check {log_filename} for details'
                )
                return ret

        self._create_mediainfo(
            output_filename,
            self._generate_output_filename(tag, 'mediainfo'),
        )
        logging.info(f'Encoding {tag} finished')
        return 0

    def _encode_remux(self):
        return self._run_ffmpeg(
            'Remux',
            '-c', 'copy',
            '-map', '0:v',
            '-map', '0:s?',
            '-map', '0:a:0',
            '-map', '0:a:0',
            '-map', '0:a:1?', '-map', '0:a:2?', '-map', '0:a:3?', '-map', '0:a:4?',
            '-map', '0:a:5?', '-map', '0:a:6?', '-map', '0:a:7?', '-map', '0:a:8?',
            '-map', '0:a:9?', '-map', '0:a:10?', '-map', '0:a:11?', '-map', '0:a:12?',
            '-map', '0:a:13?', '-map', '0:a:14?', '-map', '0:a:15?', '-map', '0:a:16?',
            '-disposition:a:1', '0',
            '-metadata:s:a:1', 'title=Doly Digital Compatibility Track',
            '-c:a:1', 'ac3', '-b:a:1', '192k',
            '-ac:a:1', '2',
            '-map_metadata', '0',
            '-map_chapters', '0',
        )

    def _encode(self, name, options):
        ffmpeg_args = []

        if self.tune:
            ffmpeg_args += ['-tune', self.tune]

        ffmpeg_args += [
            '-map', '0:v:0',
            '-map', '0:a:0',

            # Video
            '-c:v', options.encoder,
            '-crf', str(options.crf),
            '-preset', options.preset,
            '-vf', options.filters,

            # Audio
            '-c:a', options.audio_codec,
            '-ac', str(options.audio_channels),
            '-b:a', options.audio_bitrate,

            # Disable metadata
            '-map_metadata', '0',
            '-map_metadata:s:v', '-1',
            '-map_metadata:s:a', '-1',
        ]

        if self.map_subtitles:
            ffmpeg_args += ['-c:s', 'copy']
            for i, sid in enumerate(self.map_subtitles):
                ffmpeg_args += ['-map', '0:s:' + str(sid)]
                ffmpeg_args += ['-map_metadata:s:s:' + str(i), '0:s:s:' + str(sid)]
        else:
            ffmpeg_args += ['-sn']

        return self._run_ffmpeg(name, *ffmpeg_args)

    def _encode_wrapper(self, a):
        return self._encode(*a)

    def encode(self):
        logging.debug(f'Starting {self}')

        return_codes = []

        # Create mediainfo for original file
        self._create_mediainfo(
            self.input_filename,
            self._generate_output_filename('Orig', 'mediainfo'),
        )

        dest = self._generate_output_filename('Orig', self.input_filename.suffix[1:])
        if self.copy_orig:
            logging.info(f'Copying {self.input_filename} into {dest}')
            shutil.copy(self.input_filename, dest)
        elif self.move_orig:
            logging.info(f'Moving {self.input_filename} into {dest}')
            shutil.move(self.input_filename, dest)
            self.input_filename = dest
        elif self.enable_remux:
            return_codes += [self._encode_remux()]

        with multiprocessing.Pool(self.threads) as p:
            return_codes += p.map(
                self._encode_wrapper,
                [
                    (encoder, options)
                    for encoder, options in self.encoders.items()
                    if not options.skip
                ],
            )

        return max(return_codes)
